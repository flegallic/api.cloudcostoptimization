var express = require('express');
var router = express.Router();
const func = require("../function");
const jwt = require('jsonwebtoken');
//limit repeated requests to public APIs 
const rateLimit = require('express-rate-limit');
const createRateLimit = rateLimit({
	windowMs: 60 * 60 * 1000, // 1 hour
	max: 200, // Limit each IP to 100 requests per window (per 15 minutes)
	standardHeaders: true, // Return rate limit info in the RateLimit-* headers
	legacyHeaders: false, // Disable the X-RateLimit-* headers
    message: ({"error":{ "code": "429", "message":"Too many request sended from this IP, please try again after an hour"}})
});
router.use(createRateLimit);

const apiVersion = "2022-01-01"
const apiUrl = "https://api-cloudcostoptimization.azurewebsites.net/common/oauth/authorize?apiversion={'%Y-%m-%d'}";
const user = {
    id:21,
    tenantId:"01a845db-54fc-4532-bd08-87a0ea34083d",
    tenantSecret:"Xp2s5v8y/B?D(G+KbPeShVmYq3t6w9z$C&F)H@McQfTjWnZr4u7x!A%D*G-KaNdR",
    appId:"01a845db-54fc-4532-bd08-87a0ea34083d",
    appSecret:"Xp2s5v8y/B?D(G+KbPeShVmYq3t6w9z$C&F)H@McQfTjWnZr4u7x!A%D*G-KaNdR",
    appSubscription:"7c6b132c-4f0e-4b46-b48b-8ac135ee7268"
};


//whoami url
router.get("/whoami", createRateLimit, func.authenticateToken, (req,res) => {
    const user_apiversion = req.query.apiversion;
    if(user_apiversion != apiVersion) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    res.send(req.user);
});
//get access token
router.post("/common/oauth/authorize", (req,res) => {
    const user_apiversion = req.query.apiversion;
    if(user_apiversion != apiVersion) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    if(req.body.appId != user.appId) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    if(req.body.appSecret != user.appSecret) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    const accessToken  = func.generateAccessToken(user);
    const refreshToken = func.generateRefreshToken(user);
    res.send({"status":{ "code": "200", "Token": accessToken, "RefreshToken": refreshToken}, "api-version": apiVersion});
});
//get refresh token
router.post("/common/oauth/token", (req,res) => {
    const user_apiversion = req.query.apiversion;
    if(user_apiversion != apiVersion) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    const authHeaders = req.headers['authorization'];
    const token = authHeaders && authHeaders.split(' ')[1];
    if(!token) {
        res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
        return;
    }
    jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if(err){
            res.status(401).send({"error":{"code":"401 - Authentication failed","message":"Authentication is required. Invalid credentials.", "url": apiUrl}});
            return;
        }
        //check bdd if user exist and access
        delete user.iat;
        delete user.exp;
        const refreshedToken = func.generateAccessToken(user);
        res.send({ accessToken: refreshedToken });
    });
});

module.exports = router;