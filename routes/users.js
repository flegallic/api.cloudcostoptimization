var express = require('express');
var router = express.Router();

//url
router.get("/", (req,res) => {
    res.render('index');  
});
//root url
router.get('*', (req,res) => { res.status(404).send({"error":{ "code": "404", "message": "Page not found error"}}) });
router.post('*', (req,res) => { res.status(404).send({"error":{ "code": "404", "message": "Page not found error"}}) });

module.exports = router;