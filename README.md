# api.cloudcostoptimization

Nodejs Rest API example that supports token based authentication with JWT (JSONWebToken) on Azure App Service

## Getting started

- Use Thunder Client or Postman to send requests
- The body contents can be any valid JSON object, for example like this:
```
{
    "appId":"01a845db-54fc-4532-bd08-87a0ea34083d",
    "appSecret":"Xp2s5v8y/B?D(G+KbPeShVmYq3t6w9z$C&F)H@McQfTjWnZr4u7x!A%D*G-KaNdR"
}
```

- Get a token [https://api-cloudcostoptimization.azurewebsites.net/common/oauth/authorize?apiversion=2022-01-01]
- Refresh token [https://api-cloudcostoptimization.azurewebsites.net/common/oauth/token?apiversion=2022-01-01]
- Whoami url [https://api-cloudcostoptimization.azurewebsites.net/whoami?apiversion=2022-01-01]
- Welcome url [https://api-cloudcostoptimization.azurewebsites.net/]