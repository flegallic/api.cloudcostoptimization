const jwt = require('jsonwebtoken');

//function
function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"600s"})
}
function generateRefreshToken(user) {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {expiresIn:"1m"})
}
function authenticateToken(req,res,next) {
    const authHeaders = req.headers['authorization'];
    const token = authHeaders && authHeaders.split(' ')[1];
    if(!token){
        res.status(400).send({"error":{"code":"400 - Authentication failed","message":"Authentication is required. The 'Authorization' header is missing."}});
        return;
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if(err){
            res.status(400).send({"error":{"code":"400 - Authentication failed","message":"Authentication is required. The 'Authorization' header is missing."}});
            return;
        }
        req.user = user;
        next();
    });
}

module.exports = { generateAccessToken, generateRefreshToken, authenticateToken };